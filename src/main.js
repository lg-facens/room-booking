import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'

// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth, GoogleAuthProvider } from 'firebase/auth';
import { getFirestore } from 'firebase/firestore'
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyCgby_PbE7_sjjMJtwpGX5LSHjyUXhCW8c",
  authDomain: "roombooking-2fef4.firebaseapp.com",
  projectId: "roombooking-2fef4",
  storageBucket: "roombooking-2fef4.appspot.com",
  messagingSenderId: "784432085962",
  appId: "1:784432085962:web:9d394a6ad32d9626b8ed33",
  measurementId: "G-103NK1ESSY"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export const googleProvider = new GoogleAuthProvider();

export const auth = getAuth();
export const db = getFirestore(app);

Vue.config.productionTip = false


new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
