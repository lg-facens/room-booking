import Vue from 'vue'
import Vuex from 'vuex'
import { signInWithPopup } from 'firebase/auth';
import { auth, db, googleProvider } from '../main';
import {  collection, doc, getDoc,  getDocs, updateDoc } from 'firebase/firestore';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: null,
    events:[],
  },
  getters: {
  },
  mutations: {
    setUser(state, data){
      console.log(data);
      state.user = data
    },
    setEvents (state, events){
      state.events = events
    }
  },
  actions: {
    async socialLogin({commit}) {
      try  {
        // await signInWithPopup(auth, googleProvider)
        let user = await signInWithPopup(auth, googleProvider)
        commit('setUser', {id:user.user.uid, name: user.user.displayName, email: user.user.email, labs:'' })
        console.log(user)
        
      } catch(err) {
        if (err.code === 'auth/popup-closed-by-user') {
          return;
        }

        alert('Oops. ' + err.message)
      }
    },
    async getEvents({commit}){
      const snapshot= await getDocs(collection(db, "events"))
      const events = [];
      snapshot.forEach(doc => {
      console.log(doc.data());
      let appData = doc.data();
      appData.id = doc.id;
      events.push(appData);
    });
    commit('setEvents', events)
   },
    async getUserById(_, payload){
      console.log(payload);
      const ref = doc(db, 'users', payload)
      const document = await getDoc(ref)
      return document.data()
      
    },
    async updateUser({commit, state},payload){
      const ref = doc(db,'users', payload.id)
      await updateDoc(ref, {...payload.data})
      commit("setUser", {...state.user, ...payload.data})
    },
  },
  
  modules: {
  }
})
