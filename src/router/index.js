import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'
import PickerView from '../views/PickerView.vue'
import LoginView from '../views/LoginView.vue'
import FormView from '../views/FormView.vue'
import UpdateView from '../views/UpdateView.vue'
import CalendarView1 from '../views/CalendarView1.vue'
import CalendarView2 from '../views/CalendarView2.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/picker',
    name: 'picker',
    component: PickerView
  },
  {
    path: '/login',
    name:'login',
    component: LoginView
  },
  {
    path: '/form',
    name:'form',
    component: FormView
    },
    {
      path: '/update',
      name: 'update',
      component: UpdateView
    },
    {
      path:'/calendar/1',
      name:'calendar1',
      component: CalendarView1
    },
    {
      path:'/calendar/2',
      name:'calendar2',
      component: CalendarView2
    }
   
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
